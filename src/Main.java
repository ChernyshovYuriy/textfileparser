import java.io.*;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: ChernyshovYuriy
 * Date: 08.05.13
 * Time: 11:09
 */
public class Main {

    private enum DESTINATIONS {
        RESULT_DESTINATION_UNKNOWN,
        RESULT_DESTINATION_FILE,
        RESULT_DESTINATION_CONSOLE
    }

    public Main(String filePath) {
        Console console = System.console();
        if (console != null) {
            String resultDestination = console.readLine("Start file processing Y / N: ");
            if (resultDestination != null) {
                if (resultDestination.equalsIgnoreCase("Y")) {
                    processFile(filePath);
                } else if (resultDestination.equalsIgnoreCase("N")) {
                    System.exit(0);
                }
            }
        }
    }

    public static void main(String [] args) {
        if (args != null && args[0] != null) {
            new Main(args[0]);
        } else {
            System.out.println("File path not specified!");
            System.exit(-1);
        }
    }

    private void processFile(String filePath) {
        Map<String, DataVO> wordOccurrences = new HashMap<String, DataVO>();
        File file = new File(filePath);
        FileReader fileReader = null;
        BufferedReader bufferedReader = null;
        try {
            fileReader = new FileReader(file);
            bufferedReader = new BufferedReader(fileReader);
            String line;
            while((line = bufferedReader.readLine()) != null) {
                processTextLine(line, wordOccurrences);
            }
        } catch (IOException e) {
            System.out.println("Read file error: " + e);
            System.exit(-1);
        } finally {
            try {
                if (bufferedReader != null) {
                    bufferedReader.close();
                }
                if (fileReader != null) {
                    fileReader.close();
                }
            } catch (IOException ignored) {

            }
        }
        System.out.println("\nProcess is finished.");

        DESTINATIONS resultDestination = getResultDestination();
        switch (resultDestination) {
            case RESULT_DESTINATION_FILE:
                saveResultToFile(wordOccurrences);
                break;
            case RESULT_DESTINATION_CONSOLE:
                printResultToConsole(wordOccurrences);
                break;
        }

        // Just for the Test
        //printResultToConsole(wordOccurrences);
        //saveResultToFile(wordOccurrences);
    }

    private void printResultToConsole(Map<String, DataVO> wordOccurrences) {
        System.out.println("");
        for (DataVO dataVO : wordOccurrences.values()) {
            System.out.println("Occurrences: " + dataVO.getOccurrencesCount() + " word: " + dataVO.getWord());
        }
    }

    private void saveResultToFile(Map<String, DataVO> wordOccurrences) {
        StringBuilder stringBuilder = new StringBuilder();
        for (DataVO dataVO : wordOccurrences.values()) {
            stringBuilder.append("Occurrences: ");
            stringBuilder.append(dataVO.getOccurrencesCount());
            stringBuilder.append(" word: ");
            stringBuilder.append(dataVO.getWord());
            stringBuilder.append("\n");
        }

        File file = new File("");
        FileWriter fileWriter = null;
        BufferedWriter bufferedWriter = null;
        String fullFileName = file.getAbsolutePath() + "/result_data.txt";

        try {
            fileWriter = new FileWriter(fullFileName);
            bufferedWriter = new BufferedWriter(fileWriter);
            bufferedWriter.write(stringBuilder.toString());

            Console console = System.console();
            if (console != null) {
                console.readLine("File '" + fullFileName + "' has been saved");
            }
        } catch (IOException e) {
            System.out.println("Save result to file error: " + e);
        } finally {
            try {
                if (bufferedWriter != null) {
                    bufferedWriter.close();
                }
                if (fileWriter != null) {
                    fileWriter.close();
                }
            } catch (IOException ignored) {

            }
        }
    }

    private DESTINATIONS getResultDestination() {
        Console console = System.console();
        if (console == null) {
            return DESTINATIONS.RESULT_DESTINATION_UNKNOWN;
        }
        String resultDestination = console.readLine("\nSet result destination F (save in File) or C (show in Console): ");
        if (resultDestination != null) {
            if (resultDestination.equalsIgnoreCase("F")) {
                return DESTINATIONS.RESULT_DESTINATION_FILE;
            } else if (resultDestination.equalsIgnoreCase("C")) {
                return DESTINATIONS.RESULT_DESTINATION_CONSOLE;
            }
        }
        console.readLine("Specify a valid destination!");
        return getResultDestination();
    }

    private void processTextLine(String line, Map<String, DataVO> wordOccurrences) {
        if (line == null) {
            return;
        }
        // SIMPLE ref exp to split a words
        String[] words = line.split(" ");
        DataVO dataVO;
        for (String word : words) {
            // SIMPLE ref exp to remove non word's characters
            word = word.replaceAll("[\\.\\,]{1,}", "");

            if (!wordOccurrences.containsKey(word)) {
                dataVO = new DataVO(word);
                wordOccurrences.put(word, dataVO);
            } else {
                dataVO = wordOccurrences.get(word);
                dataVO.incrementOccurrencesCount();
            }
        }
    }

    private class DataVO {

        private int occurrencesCount = 1;
        private String word = "";

        private DataVO(String word) {
            this.word = word;
        }

        private String getWord() {
            return word;
        }

        public int getOccurrencesCount() {
            return occurrencesCount;
        }

        public void incrementOccurrencesCount() {
            occurrencesCount++;
        }
    }
}